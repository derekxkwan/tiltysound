var base_alpha, base_beta, base_gamma, cur_alpha, cur_beta, cur_gamma;

function handleOri(evt){
    cur_alpha = evt.alpha; //around z axis
    cur_beta = evt.beta; //around x axis
    cur_gamma = evt.gamma; //around y axis
   disp_alpha.innerHTML = cur_alpha;
   disp_beta.innerHTML = cur_beta;
   disp_gamma.innerHTML = cur_gamma;
};


window.addEventListener("deviceorientation", handleOri, true);
